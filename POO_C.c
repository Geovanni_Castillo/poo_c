#include <stdio.h>
#include <stdlib.h>
#include <String.h>

struct employee{
	int experience;
	double basic_salary;
	double salary;
	//char name[20];
	int (*calculate_salary) (int a);
};

typedef struct employee emp;
typedef emp *Employee;

int cal_salary(Employee exp_em){
	int salary=0;
	if(exp_em->experience>15)
		salary+=25000;
	else if(exp_em->experience>10)
		salary+=15000;
	else if (exp_em->experience>5)
		salary+=5000;
	return salary;
}

int cal_salary_oop(int exp){
	int salary=0;
	if(exp>15)
		salary+=25000;
	else if (exp>10)
		salary+=15000;
	else if(exp>5)
		salary+=5000;
	return salary;
}

Employee new_Employee(int experience, int basic_salary){
	Employee obj=(Employee)malloc(sizeof(emp));
	obj->basic_salary=basic_salary;
	obj->experience=experience;
	obj->calculate_salary=cal_salary_oop(experience);
	return obj;
}

void destroy_Employee(Employee e){
	free(e);
}

int main(){
	Employee emp1=new_Employee(14,2000);
	Employee emp2=new_Employee(7,1500);
	
	int emp1_sal=emp1->basic_salary;
	int emp2_sal=emp2->basic_salary;
	int emp1_cal_salary=cal_salary(emp1);
	int emp2_cal_salary=cal_salary(emp2);
	
	printf("Empleado 1 Salario Basico %d\n", emp1_sal);
	printf("Empleado 2 Salario Basico %d\n", emp2_sal);
	printf("Empleado 1 Calcular Salario %d\n", emp1_cal_salary);
	printf("Empleado 2 Calcular Salario %d\n", emp2_cal_salary);
	
	emp1_cal_salary=emp1->calculate_salary;
	printf("Empleado 1 Calcular Salario con POO %d\n", emp1_cal_salary);
	
	emp2_cal_salary=emp2->calculate_salary;
	printf("Empleado 2 Calcular Salario con POO %d\n", emp2_cal_salary);
	
	destroy_Employee(emp1);
	destroy_Employee(emp2);
	
	return 0;
	
}
